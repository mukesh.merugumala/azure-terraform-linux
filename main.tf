
resource "azurerm_resource_group" "kube" {
  name = "my-first-terraform-kube"
  location = "eastus"
}

resource "azurerm_virtual_network" "myvnet" {
  name = "my-vnet"
  address_space = ["10.0.0.0/16"]
  location = "eastus"
  resource_group_name = azurerm_resource_group.kube.name
}

resource "azurerm_subnet" "frontendsubnet" {
  name = "frontendSubnet"
  resource_group_name =  azurerm_resource_group.kube.name
  virtual_network_name = azurerm_virtual_network.myvnet.name
  address_prefix = "10.0.1.0/24"
}

resource "azurerm_public_ip" "myvm1publicip" {
  name = "pip1"
  location = "eastus"
  resource_group_name = azurerm_resource_group.kube.name
  allocation_method = "Dynamic"
  sku = "Basic"
}

resource "azurerm_public_ip" "myvm1publicip1" {
  name = "pip"
  location = "eastus"
  resource_group_name = azurerm_resource_group.kube.name
  allocation_method = "Dynamic"
  sku = "Basic"
}

resource "azurerm_network_interface" "myvm1nic" {
  name = "myvm1-nic"
  location = "eastus"
  resource_group_name = azurerm_resource_group.kube.name

  ip_configuration {
    name = "ipconfig1"
    subnet_id = azurerm_subnet.frontendsubnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.myvm1publicip.id
  }
}

resource "azurerm_network_interface" "myvm1nic1" {
  name = "myvm1-nic1"
  location = "eastus"
  resource_group_name = azurerm_resource_group.kube.name

  ip_configuration {
    name = "ipconfig2"
    subnet_id = azurerm_subnet.frontendsubnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.myvm1publicip1.id
  }
}

resource "azurerm_linux_virtual_machine" "myterraformvm" {
    name                  = "kubernetes-server"
    location              = "eastus"
    resource_group_name   = azurerm_resource_group.kube.name
    network_interface_ids = [azurerm_network_interface.myvm1nic.id]
    size                  = "Standard_B2s"

    os_disk {
      caching           = "ReadWrite"
      storage_account_type = "Standard_LRS"
    }

    source_image_reference {
        publisher = "OpenLogic"
        offer     = "CentOS"
        sku       = "7.3"
        version   = "latest"
    }

    computer_name  = "myvm"
    admin_username = "azureuser"
    admin_password = "Password123!"
	disable_password_authentication = false


    tags = {
        environment = "Terraform Demo"
    }
}

resource "azurerm_linux_virtual_machine" "myterraformvm1" {
    name                  = "kubernetes-client-1"
    location              = "eastus"
    resource_group_name   = azurerm_resource_group.kube.name
    network_interface_ids = [azurerm_network_interface.myvm1nic1.id]
    size                  = "Standard_B2s"

    os_disk {
      caching           = "ReadWrite"
      storage_account_type = "Standard_LRS"
    }

    source_image_reference {
        publisher = "OpenLogic"
        offer     = "CentOS"
        sku       = "7.3"
        version   = "latest"
    }

    computer_name  = "myvm"
    admin_username = "azureuser"
    admin_password = "Password123!"
	disable_password_authentication = false


    tags = {
        environment = "Terraform Demo"
    }
}

